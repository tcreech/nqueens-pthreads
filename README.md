nqueens for pthreads (via static scheduling), starting from an OpenMP version by Kenji KISE. Set number of threads as if it were an actual OpenMP program:

OMP_NUM_THREADS=16 ./nqueens-pthreads 10

